/**
 * uniir - a universal infrared remote (learning project)
 * 
 * this is a partial (learning) project which I will combine with some other 
 * parts into a learning infrared remote with a web interface. It will be able  
 * to read / store its configuration from / to a RESTful WebService.
 * 
 * This project can store a string into a RESTful WebService and load from it.
 * 
 * To try this:
 * 1. Create a WebService which can process GET / POST requests, or just use
 *    unir-php-rest-service-test.
 * 2. Configure the Configuration.h so it fits your network / REST Service
 * 3. Download on your arduino
 * 4. Start the serial monitor and send 's' for storing the file and 'l' 
 *    for loading the file's content 
 * 
 * @author Manuel Manhart
 */
#include <SPI.h>
#include <Ethernet.h>
#include "HttpClient.h"
#include "Configuration.h"

static HttpClient http;

//////////////////////

void setup() {
	Serial.begin(9600);
	while (!Serial) {
		; // wait for serial port to connect. Needed for Leonardo only
	}

	http.begin();
	http.loadConfig();
	printHelp();
}

void loop() {
	// check for serial input
	if (Serial.available() > 0) { // if something in serial buffer
		byte inChar; // sets inChar as a byte
		inChar = Serial.read(); //gets byte from buffer
		if (inChar == 'l') { // check if we want to load from the rest service
			http.loadConfig();
			printHelp();
		} else if (inChar == 's') { // check if we want to save to the rest service
			http.saveConfig(); // call sendGET function below when byte is an e
			printHelp();
		}
	}
}

void printHelp() {
	Serial.println();
	Serial.println("==================");
	Serial.println(
			"This sketch shows how to load a JSON config from a RESTful API (send 'l' over serial) and");
	Serial.println(
			"                  how to save a JSON config to a RESTful API (send 's' over serial)");
	Serial.println("==================");
	Serial.println();
}
