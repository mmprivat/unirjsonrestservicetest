UNIR
=====

a universal infrared remote (learning project)

### What is this repository for? ###

this is a partial (learning) project which I will combine with some other 
parts into a learning infrared remote with a web interface. 
It will be able to read / store its configuration from / to a RESTful 
WebService.

This project can store a string into a RESTful WebService and load from it.

### How do I get set up? ###

1. Create a WebService which can process GET / POST requests, or just use `unir-php-rest-service-test`.
2. Configure the Configuration.h so it fits your network / REST Service
3. Download on your arduino

### Testing ###
Start the serial monitor and send 's' for storing the file and 'l' for loading the file's content