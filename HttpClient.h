#ifndef _HTTPCLIENT_H
#define _HTTPCLIENT_H
#include <Ethernet.h>

class HttpClient {
  public:
    void begin();
    void loadConfig();
    void saveConfig();

  private:
    EthernetClient client;

};
#endif
