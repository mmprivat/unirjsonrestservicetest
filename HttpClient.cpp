#include <SPI.h>
#include <Ethernet.h>
#include "HttpClient.h"
#include "Configuration.h"

//////////////////////

void HttpClient::begin() {
	delay(100);
	byte mac[] = CONFIG_MAC_ADDRESS; //physical mac address
	IPAddress ip = IPAddress(10, 0, 0, 15); // ip in lan assigned to arduino

	Ethernet.begin(mac, ip);
	Serial.print("Address=");
	Serial.println(Ethernet.localIP());
	Serial.print("Subnet=");
	Serial.println(Ethernet.subnetMask());
	Serial.print("DNS=");
	Serial.println(Ethernet.dnsServerIP());
}

//////////////////////////

void HttpClient::loadConfig() { // client function to send/receive GET request data.

	IPAddress serverIp = CONFIG_SERVER_NAME; // zoomkat web page server IP address
	if (client.connect(serverIp, CONFIG_SERVER_PORT)) { //starts client connection, checks for connection
		Serial.println("connected");
		client.println("GET " CONFIG_API_PATH " HTTP/1.0"); //download text
		client.println(); //end of get request
	} else {
		Serial.println("connection failed"); //error message if no client connect
		Serial.println();
	}

	while (client.connected() && !client.available())
		delay(1); //waits for data
	while (client.connected() || client.available()) { //connected or data available
		char c = client.read(); //gets byte from ethernet buffer
		Serial.print(c); //prints byte to serial monitor
	}
	client.stop(); //stop client
}

void HttpClient::saveConfig() { //client function to send/receive GET request data.
	IPAddress serverIp = CONFIG_SERVER_NAME; // zoomkat web page server IP address
	String c = "{\"ir\": {\"Einschalten\": \"#EEEEEE\"}, {\"Ausschalten\": \"#AAAAAA\"} }";
	String cLength = "Content-Length: ";
	cLength = cLength + c.length();
	if (client.connect(serverIp, CONFIG_SERVER_PORT)) { //starts client connection, checks for connection
		Serial.println("connected");
		client.println("POST " CONFIG_API_PATH " HTTP/1.0"); //download text
		client.println(cLength);
		client.println(); // body
		client.println(c); // See more at: http://restcookbook.com/HTTP%20Methods/put-vs-post/#sthash.q0Tihqlw.dpuf
		Serial.println("DEBUG cLength: ");
		Serial.println(cLength);
	} else {
		Serial.println("connection failed"); //error message if no client connect
		Serial.println();
	}

	while (client.connected() && !client.available())
		delay(1); //waits for data
	while (client.connected() || client.available()) { //connected or data available
		char c = client.read(); //gets byte from ethernet buffer
		Serial.print(c); //prints byte to serial monitor
	}

	Serial.println();
	Serial.println("disconnecting.");
	Serial.println("==================");
	Serial.println();
	client.stop(); //stop client
}
