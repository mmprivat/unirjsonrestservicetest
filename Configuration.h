#ifndef _CONFIGURATION_H
#define _CONFIGURATION_H
#define CONFIG_MAC_ADDRESS { 0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED }
//#define CONFIG_SERVER_NAME "arduino.cc"
#define CONFIG_SERVER_NAME IPAddress(10,0,0,5)
#define CONFIG_IP_ADDRESS IPAddress(10,0,0,15)
#define CONFIG_GATEWAY_ADDRESS IPAddress(10,0,0,138)
#define CONFIG_SERVER_PORT 80
// TODO Create a webform where one can put a url for loading / saving and
// a textarea for directly putting the config
#define CONFIG_API_PATH "/rest/datastore/uniir2.cfg"
#endif
